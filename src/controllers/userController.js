const express = require('express');
const router = new express.Router();

const {getProfileByUserId} = require('../services/userServices');
const {changeUserPassword} = require('../services/userServices');
const {deleteProfileByUserId} = require('../services/userServices');
const {asyncWrapper} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const profile = await getProfileByUserId(userId);

  res.status(200).json({user: profile});
}));


router.patch('/password', asyncWrapper(async (req, res) => {
  const {
    oldPassword,
    newPassword,
  } = req.body;
  const {userId} = req.user;

  await changeUserPassword(userId, oldPassword, newPassword);

  res.status(200).json({massege: 'Password changed successfully'});
}));


router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteProfileByUserId(userId);

  res.status(200).json({message: 'Profile deleted successfully'});
}));

module.exports = {
  userController: router,
};
