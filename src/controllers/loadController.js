const express = require('express');
const router = new express.Router();

const {isShipperMiddleware} =require('../middlewares/isShipperMiddleware');
const {isDriverMiddleware} =require('../middlewares/isDriverMiddleware');
const {asyncWrapper} = require('../utils/apiUtils');
const {
  addLoadForUser,
  getUserLoads,
  getUserActiveLoad,
  changeLoadStateToNext,
  getUserLoadById,
  updateLoadById,
  deleteLoadById,
} = require('../services/loadServices');
const {findeTruckForLoad} = require('../services/systemServices');


router.post('/', isShipperMiddleware, asyncWrapper(async (req, res) => {
  await addLoadForUser(req.body, req.user.userId);
  res.status(200).json({message: 'Load created successfully'});
}));


router.get('/', asyncWrapper(async (req, res) => {
  const {role} = req.user;
  const {userId} = req.user;

  const loads = await getUserLoads(userId, role);
  res.status(200).json({loads: loads});
}));


router.get('/active', isDriverMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const load = await getUserActiveLoad(userId);
  res.status(200).json({load: load});
}));


router.patch('/active/state', isDriverMiddleware,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const actuallState = await changeLoadStateToNext(userId);
      res.status(200).json({message:
    `Load state changed to ${actuallState}`});
    }));


router.get('/:id', isShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const load = await getUserLoadById(req.params.id, userId);
  res.status(200).json({load: load});
}));


router.put('/:id', isShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await updateLoadById(req.params.id, userId, req.body);
  res.status(200).json({message: 'Load details changed successfully'});
}));


router.delete('/:id', isShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteLoadById(req.params.id, userId);
  res.status(200).json({message: 'Load deleted successfully'});
}));


router.get('/:id/shipping_info', isShipperMiddleware,
    asyncWrapper(async (req, res) => {
      const load = await getUserLoadById(req.params.id);
      res.status(200).json({load: load});
    }));


router.post('/:id/post', isShipperMiddleware, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const driverFound = await findeTruckForLoad(req.params.id, userId);
  let message = 'Load posted successfully';
  if (!driverFound) {
    message = 'Load post fail';
  }
  res.status(200).json({
    message: message,
    driver_found: driverFound,
  });
}));


module.exports = {
  loadController: router,
};
