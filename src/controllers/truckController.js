const express = require('express');
const router = new express.Router();


const {asyncWrapper} = require('../utils/apiUtils');
const {
  addTruckForUser,
  getUserTrucks,
  getUserTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../services/truckServices');


router.post('/', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {type} = req.body;
  await addTruckForUser(userId, type);
  res.status(200).json({message: 'Truck created successfully'});
}));


router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const trucks = await getUserTrucks(userId);
  res.status(200).json({trucks: trucks});
}));


router.get('/:id', asyncWrapper(async (req, res) => {
  const truck = await getUserTruckById(req.params.id);
  res.status(200).json({truck: truck});
}));


router.put('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {type} = req.body;
  await updateTruckById(req.params.id, userId, {type: type});
  res.status(200).json({message: 'Truck details changed successfully'});
}));


router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteTruckById(req.params.id, userId);
  res.status(200).json({message: 'Truck deleted successfully'});
}));


router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await assignTruckById(req.params.id, userId);
  res.status(200).json({message: 'Truck assigned successfully'});
}));


module.exports = {
  truckController: router,
};
