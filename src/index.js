require('dotenv').config();
const port = process.DB_PORT || 8080;
const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);


const {authController} = require('./controllers/authController');
const {authMiddleware} =require('./middlewares/authMiddleware');
const {isDriverMiddleware} =require('./middlewares/isDriverMiddleware');
const {userController} = require('./controllers/userController');
const {loadController} = require('./controllers/loadController');
const {truckController} = require('./controllers/truckController');


app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authController);
app.use(authMiddleware);

app.use('/api/users/me', userController);
app.use('/api/loads', loadController);
app.use('/api/trucks', isDriverMiddleware, truckController);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://Af13:ZYSq56FPzQ6hnsJ@cluster0.tybje.mongodb.net/shipmentProdjectTest2?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true,
    });
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
