const {nextOneFromArray} = require('../utils/apiUtils');
const {
  LOAD_STATUSES,
  TRUCK_TYPES,
  TRUCK_STATUSES,
} = require('../utils/detailsData');

const {
  getAllISTrucks,
  updateTruckById,
} = require('./truckServices');
const {
  getUserLoadById,
  postUserLoadById,
} = require('./loadServices');


const chooseTruckType = (loadDimensious, payload, startFrom = 0) => {
  let appropriateType;
  const truckTypes = Object.entries(TRUCK_TYPES);

  for (let i = startFrom; i < truckTypes.length; i++) {
    if (truckTypes[i][1].payload < payload) {
      continue;
    }
    if (
      truckTypes[i][1].longitude > loadDimensious.length &&
      truckTypes[i][1].height > loadDimensious.height &&
      truckTypes[i][1].width > loadDimensious.width
    ) {
      appropriateType = Object.keys(TRUCK_TYPES);
      break;
    }
  }

  return appropriateType;
};


const findeTruckForLoad = async (loadId, userId) => {
  const {
    dimensions,
    payload,
    status,
  } = await getUserLoadById(loadId, userId);
  if (LOAD_STATUSES.indexOf(status) !== 0) {
    throw new Error('Only load with NEW state can be POSTED');
  }
  const type = await chooseTruckType(dimensions, payload);
  const foundTrucks = await getAllISTrucks(type);
  const truck = await foundTrucks[0];
  const driverId = truck.created_by;
  await postUserLoadById(loadId, userId, status, driverId);
  await updateTruckById(truck._id, driverId, {
    status: await nextOneFromArray(truck.status, TRUCK_STATUSES),
    assigned_to: truck.created_by,
  });

  return Boolean(truck);
};


module.exports = {
  findeTruckForLoad,
};
