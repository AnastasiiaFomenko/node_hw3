const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getProfileByUserId = async (userId) => {
  return await User.findById(userId, '-__v -password');
};


const changeUserPassword = async (userId, oldPassword, newPassword) => {
  const user = await User.findById(userId);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password');
  }

  await User.findByIdAndUpdate(userId, {
    password: await bcrypt.hash(newPassword, 10),
  });
};


const deleteProfileByUserId = async (userId) => {
  return await User.findByIdAndRemove(userId);
};


module.exports = {
  getProfileByUserId,
  changeUserPassword,
  deleteProfileByUserId,
};
