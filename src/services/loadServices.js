const {Load} = require('../models/loadModel');
const {LOAD_STATES, LOAD_STATUSES} = require('../utils/detailsData');
const {nextOneFromArray} = require('../utils/apiUtils');


const addLoadForUser = async (reqBody, userId) => {
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = reqBody;
  const load = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: userId,
  });
  await load.save();
};


const getUserLoads = async (userId, role) => {
  if (role === 'SHIPPER') {
    return Load.find({created_by: userId});
  }

  if (role === 'DRIVER') {
    return Load.find({assigned_to: userId});
  }
};


const getUserActiveLoad = async (userId) => {
  const load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
  return load;
};


const changeLoadStateToNext = async (userId) => {
  const {
    state,
    status,
    _id,
  } = await getUserActiveLoad(userId);
  let newStatus = null;
  if (!_id) {
    throw new Error('No ASSIGNED loads');
  }
  const nextState = await nextOneFromArray(state, LOAD_STATES);
  if (!nextState) {
    throw new Error('State `Arrived to delivery` cannot be changed');
  }
  if (LOAD_STATES.indexOf(nextState) === LOAD_STATES.length -1) {
    newStatus = LOAD_STATUSES[LOAD_STATUSES.length-1];
  }

  await Load.findByIdAndUpdate(_id, {$set: {
    state: nextState,
    status: newStatus ? newStatus : status,
  }});
  return nextState;
};


const getUserLoadById = async (loadId, userId) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});
  return load;
};


const updateLoadById = async (loadId, userId, updateData) => {
  const load = await Load.findOneAndUpdate({_id: loadId, created_by: userId},
      {$set: updateData});
  return load;
};


const deleteLoadById = async (loadId, userId) => {
  await Load.findOneAndDelete({_id: loadId, created_by: userId});
};


const postUserLoadById = async (loadId, userId, status, driverId) => {
  if (driverId) {
    await updateLoadById(loadId, userId, {
      status: await nextOneFromArray(status, LOAD_STATUSES),
    });
    await updateLoadById(loadId, userId, {
      status: await nextOneFromArray(status, LOAD_STATUSES, 1),
      state: LOAD_STATES[0],
      assigned_to: driverId,
      logs: [
        {
          message: `Load assigned to driver with id ${driverId}`,
          time: Date.now(),
        },
      ],
    });
  } else {
    await updateLoadById(loadId, userId, {
      logth: [
        {
          message: `Load post faile`,
          time: Date.now(),
        }],
    });
  }
};


module.exports = {
  addLoadForUser,
  getUserLoads,
  getUserActiveLoad,
  changeLoadStateToNext,
  getUserLoadById,
  updateLoadById,
  deleteLoadById,
  postUserLoadById,
};
