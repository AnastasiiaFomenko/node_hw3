const {Truck} = require('../models/truckModel');
const {TRUCK_STATUSES} = require('../utils/detailsData');

const addTruckForUser = async (userId, type) => {
  const truck = new Truck({
    type,
    created_by: userId,
  });

  await truck.save();
};


const getUserTrucks = async (userId) => {
  return Truck.find({created_by: userId});
};


const getUserTruckById = (truckId) => {
  return Truck.findById(truckId);
};


const updateTruckById = async (truckId, userId, updateData) => {
  await Truck.findOneAndUpdate({_id: truckId, created_by: userId},
      {$set: updateData});
};


const deleteTruckById = async (truckId, userId) => {
  await Truck.findOneAndDelete({_id: truckId, created_by: userId});
};


const getAllISTrucks = async (type) => {
  return Truck.find({status: TRUCK_STATUSES[0], type: type});
};

const assignTruckById = async (truckId, userId) => {
  await Truck.findOneAndUpdate({
    _id: truckId,
    created_by: userId,
    status: TRUCK_STATUSES[0],
  },
  {$set: {
    assigned_to: userId,
  }});
};

module.exports = {
  addTruckForUser,
  getUserTrucks,
  getUserTruckById,
  updateTruckById,
  deleteTruckById,
  getAllISTrucks,
  assignTruckById,
};
