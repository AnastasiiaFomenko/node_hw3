const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    required: true,
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  status: {
    type: String,
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
  },
  logs: [{message: String, time: Date}],
});

module.exports = {Load};
