const mongoose = require('mongoose');

const User = mongoose.model('User', {
  role: {
    required: true,
    type: String,
  },
  email: {
    required: true,
    type: String,
    unique: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {User};
