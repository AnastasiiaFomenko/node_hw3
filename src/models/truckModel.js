const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    required: true,
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  status: {
    type: String,
    default: 'IS',
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
});

module.exports = {Truck};
