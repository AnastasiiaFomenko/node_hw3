const TRUCK_TYPES = {
  SPRINTER: {
    longitude: 300,
    height: 250,
    width: 170,
    payload: 1700,
  },
  SMALL_STRAIGHT: {
    longitude: 500,
    height: 250,
    width: 170,
    payload: 2500,
  },
  LARGE_STRAIGHT: {
    longitude: 700,
    height: 350,
    width: 200,
    payload: 4000,
  },
};


const TRUCK_STATUSES = [
  'IS',
  'OL',
];


const LOAD_STATES = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route yo delivery',
  'Arrived to delivery',
];

const LOAD_STATUSES = [
  'NEW',
  'POSTED',
  'ASSIGNED',
  'SHIPPED',
];


module.exports = {
  TRUCK_TYPES,
  TRUCK_STATUSES,
  LOAD_STATES,
  LOAD_STATUSES,
};
