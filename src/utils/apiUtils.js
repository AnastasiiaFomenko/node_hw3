const asyncWrapper = (callback) => {
  return (req, res, next) => {
    callback(req, res)
        .catch(next);
  };
};


const nextOneFromArray = (current, array, moveTo = 0) => {
  const currentIndex = array.indexOf(current);
  if (currentIndex > -1 && currentIndex+1 !== array.length) {
    return array[currentIndex+(1+moveTo)];
  }
};


module.exports = {
  asyncWrapper,
  nextOneFromArray,
};
