const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required()
        .email(),
    role: Joi.string().valid('SHIPPER', 'DRIVER'),
    password: Joi.string()
        .min(6)
        .max(20)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};


module.exports = {
  registrationValidator,
};
