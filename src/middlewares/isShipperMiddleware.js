const isShipperMiddleware = (req, res, next) => {
  const {role} = req.user;
  if (role !== 'SHIPPER') {
    res.status(401).json({message: 'No acccess'});
  }
  next();
};

module.exports = {
  isShipperMiddleware,
};
