const isDriverMiddleware = (req, res, next) => {
  const {role} = req.user;
  console.log(role);
  if (role !== 'DRIVER') {
    throw new Error('No acccess');
  }
  next();
};

module.exports = {
  isDriverMiddleware,
};
